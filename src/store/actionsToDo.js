import axios from '../axios-orders';


export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_ERROR = 'FETCH_POST_ERROR';
export const SAVE_POST_ERROR = 'SAVE_POST_ERROR';

export const fetchPostRequest = () => {
  return {type: FETCH_POST_REQUEST};
};

export const fetchPostSuccess = tasks => {
  return {type: FETCH_POST_SUCCESS, tasks};
};

export const fetchPostError = () => {
  return {type: FETCH_POST_ERROR};
};

export const savePostError = () => {
  return {type: SAVE_POST_ERROR};
};

export const fetchPost = () => {
  return dispatch => {
    dispatch(fetchPostRequest());
    axios.get('/posts.json').then(response => {
      dispatch(fetchPostSuccess(response.data));
    }, error => {
      dispatch(fetchPostError());
    });
  }
};

export const savePost = (post) => {
  console.log(post);
  return dispatch => {
    axios.post('/posts.json', post).then(() => {
      dispatch(fetchPost())
    }, error => {
      dispatch(savePostError());
    });
  }
};

export const removePost = (id) => {
  return (dispatch) => {
    axios.delete('posts/' + id + '.json').then(() => {
      dispatch(fetchPost())
    })
  }
};