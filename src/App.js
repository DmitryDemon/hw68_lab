import React, {Component} from 'react';

import './App.css';
import Counter from "./containers/Counter/Counter";
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router";
import ToDo from "./containers/ToDo/ToDo";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Counter} />
          <Route path="/todo" component={ToDo} />
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
