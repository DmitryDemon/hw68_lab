import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-chernyavskii.firebaseio.com/'
});

// instance.interceptors.request.use(req => {
//   console.log('[In request interceptor]', req);
//   return req;
// });



export default instance;