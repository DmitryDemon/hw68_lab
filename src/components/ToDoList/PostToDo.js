import React from 'react';
import './ToDo.css';

const PostToDo = (props) =>  {
  return (
          props.tasks ?

            Object.keys(props.tasks).map((task) => {
              const oneTask = props.tasks[task];
                return (
                    <div className='task-todo' key={task}>
                        <p>{oneTask.text}</p>
                        <button onClick={() => props.remove(task)} className='btn-remove-todo'>Remove</button>
                    </div>
                )
            })
            : <p>this will me tasks</p>
        )

};

export default PostToDo;