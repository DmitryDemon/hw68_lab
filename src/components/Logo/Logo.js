import React from 'react';

import './Logo.css';
import burgerLogo from '../../assets/images/logo.jpg';

const Logo = () => (
  <div className="Logo">
    <img src={burgerLogo} alt="MyBurger" />
  </div>
);


export default Logo;