import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/" exact>Counter</NavigationItem>
    <NavigationItem to="/todo">ToDo</NavigationItem>
  </ul>
);


export default NavigationItems;