import React, { Component } from 'react';
import {connect} from 'react-redux';

import AddFormToDo from '../../components/ToDoList/AddFormToDo';
import {fetchPost, removePost, savePost} from "../../store/actionsToDo";
import Spinner from "../../components/UI/Spinner/Spinner";
import PostToDo from "../../components/ToDoList/PostToDo";

import './ToDoList.css';

class ToDoList extends Component {

  state = {
    textInput: '',
  };

  addPostHandler = () => {
    const post = {text: this.state.textInput};
    this.props.savePost(post)
  };

  componentDidMount() {
    this.props.fetchPost();
  }

  changeHandler = (event) => {
    this.setState({
      textInput: event.target.value
    })
  };

  render() {
    return (
      <div className="ToDoList">
        <AddFormToDo change={(event) => this.changeHandler(event)} add={this.addPostHandler}/>
        {this.props.loading ? <Spinner />  : <PostToDo remove={this.props.removePost} tasks={this.props.tasks}/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    loading: state.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    savePost: (post) => dispatch(savePost(post)),
    fetchPost: () => dispatch(fetchPost()),
    removePost: (id) => dispatch(removePost(id)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
